﻿Imports System.Text
Imports FizzBuzzApp
Imports Microsoft.VisualStudio.TestTools.UnitTesting

#Disable Warning IDE1006 ' 命名スタイル
<TestClass()> Public Class FizzBuzzTest


    <TestClass> Public Class 三の倍数の場合
        <TestMethod()> Public Sub _3を渡したら文字列Fizzを返す()
            Dim fizzbuzz As New FizzBuzz
            Assert.AreEqual("Fizz", fizzbuzz.ToFizzBuzzString(3))
        End Sub

    End Class

    <TestClass> Public Class 五の倍数の場合
        <TestMethod()> Public Sub _5を渡したら文字列Buzzを返す()
            Dim fizzbuzz As New FizzBuzz
            Assert.AreEqual("Buzz", fizzbuzz.ToFizzBuzzString(5))
        End Sub
    End Class

    <TestClass> Public Class その他の場合
        <TestMethod()> Public Sub _1を渡したら文字列1を返す()
            Dim fizzbuzz As New FizzBuzz
            Assert.AreEqual("1", fizzbuzz.ToFizzBuzzString(1))
        End Sub
    End Class
End Class
#Enable Warning IDE1006 ' 命名スタイル