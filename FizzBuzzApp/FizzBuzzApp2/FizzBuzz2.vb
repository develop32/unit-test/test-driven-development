﻿''' <summary>
''' FizzBuzz2クラス。
''' </summary>
Public Class FizzBuzz2
    Public Function Convert(num As Integer) As String
        If num Mod 3 = 0 Then
            Return "Fizz"
        End If
        If num Mod 5 = 0 Then
            Return "Buzz"
        End If
        Return num.ToString
    End Function
End Class
