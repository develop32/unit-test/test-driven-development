# テスト駆動開発(TDD)



## Why?

「<font color="red">**動作するきれいなコード**</font>」を実装する為。



## How?

### 方法は二つ

1. <font color="orange">**きれいに実装するための設計をして、動作するコードを実装する。**</font>
2. <font color="blue">**汚いコードで実装して、後できれいにする。**</font>

<img src="H:\Repo\GitLab\test-driven-development\README.assets\image-20200905172914891.png" alt="image-20200905172914891" style="zoom:80%;" />



### テスト駆動開発のサイクル

1. テストリスト(todoリスト)を書く
   テスト仕様書のようなもの
2. そのリストから一つ選びテストコードを書く
   重要度の高いものからでも、難易度が低いものからでもOK
   コンパイルエラーを無くす
3. そのテストを実行して失敗させる(<font color="red">**Red**</font>)
4. テストをクリアする最小限プロダクトコードを書く
   汚いコードでもOK
5. 2で書いたテストを成功させる(<font color="green">**Green**</font>)
6. テストが通るままでリファクタリング(<font color="orange">**Refactor**</font>)
7. 1～6を繰り返す

<img src="H:\Repo\GitLab\test-driven-development\README.assets\image-20200905191215855.png" alt="image-20200905191215855" style="zoom:80%;" />

リファクタリングは、しなくても正常に動くため、軽視されがち。
しかし、長期的に考えて改修容易性の向上や不具合混入を減らす事ができる為、とても重要。



## テストの全体像

![image-20200905193416138](H:\Repo\GitLab\test-driven-development\README.assets\image-20200905193416138.png)





## デモ

![image-20200905194059011](H:\Repo\GitLab\test-driven-development\README.assets\image-20200905194059011.png)



## まとめ

- 問題(仕様)を小さく分割して、Todoリストを作る
- 重要度や簡単度合いでソートする
- 流れ
  - テスト　→　仮実装　→　三角測量　→　実装
  - テスト　→　明白な実装
- リファクタリング
- テストの構造化