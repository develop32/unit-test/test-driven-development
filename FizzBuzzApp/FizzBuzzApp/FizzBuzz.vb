﻿''' <summary>
''' FizzBuzzクラス。
''' </summary>
''' <remarks> 
''' 改定履歴<br/>
''' <list type="table">
'''   <listheader>
'''     <term>Ver - 改定日付 - 改定者</term><description>改定内容</description>
'''   </listheader>
'''   <item><term>1.00 - 2020/10/04 - ああああ</term><description>【No.1】いいいい</description></item>
'''   <item><term>2.00 - 2020/10/04 - ああああ</term><description>【No.1】いいいい</description></item>
'''   <item><term>3.00 - 2020/10/04 - ああああ</term><description>【No.1】いいいい</description></item>
''' </list>
''' </remarks> 
Public Class FizzBuzz
    ''' <summary>
    ''' 数値をFizzBuzz文字列に変換します。
    ''' </summary>
    ''' <param name="num">数値</param>
    ''' <returns>FizzBuzz文字列</returns>
    ''' <example>
    ''' <code>
    ''' Dim retVal As String
    ''' dim fb As New FizzBuzz
    ''' retVal = fb.ToFizzBuzzString(1)  ' 戻り値: 1
    ''' retVal = fb.ToFizzBuzzString(3)  ' 戻り値: Fizz
    ''' retVal = fb.ToFizzBuzzString(5)  ' 戻り値: Buzz
    ''' retVal = fb.ToFizzBuzzString(15) ' 戻り値: FizzBuzz
    ''' </code>
    ''' </example>
    Public Function ToFizzBuzzString(num As Integer) As String
        If num Mod 3 = 0 Then
            Return "Fizz"
        End If
        If num Mod 5 = 0 Then
            Return "Buzz"
        End If
        Return num.ToString
    End Function
End Class
